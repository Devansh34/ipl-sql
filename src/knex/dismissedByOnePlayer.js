const knex = require("./connectionToDatabase");

knex
  .select("player_dismissed")
  .select("bowler")
  .count("* as count")
  .from("deliveries")
  .where("player_dismissed", "<>", "")
  .whereNot("dismissal_kind", "like", "run out")
  .groupBy("player_dismissed", "bowler")
  .orderBy("count", "desc")
  .limit('1')
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.error(err);
  })
  .finally(() => {
    knex.destroy();
  });
