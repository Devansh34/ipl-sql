const knex = require("./connectionToDatabase");

knex
  .select("batsman")
  .select("matches.season")
  .select(
    knex.raw(
      "(SUM(batsman_runs)/NULLIF((COUNT(*) - SUM(CASE WHEN wide_runs > 0 THEN 1 ELSE 0 END)), 0))*100 AS strike_rate"
    )
  )
  .from("deliveries")
  .join("matches", "deliveries.match_id", "matches.id")
  .groupBy("batsman", "season")
  .orderBy("season")
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  })
  .finally(() => {
    knex.destroy();
  });
