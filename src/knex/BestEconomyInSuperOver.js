const knex=require('./connectionToDatabase')

knex 
.select('bowler')
.select(knex.raw('SUM(total_runs)-SUM(legbye_runs)-SUM(bye_runs) AS runs'))
.select(knex.raw('COUNT(*) - SUM(CASE WHEN noball_runs>0 OR wide_runs>0 THEN 1 ELSE 0 END) AS balls'))
.select(knex.raw('((SUM(total_runs)-SUM(legbye_runs)-SUM(bye_runs))/((COUNT(*) - SUM(CASE WHEN noball_runs>0 OR wide_runs>0 THEN 1 ELSE 0 END))/6)) AS ECONOMY'))
.from('deliveries')
.join('matches','deliveries.match_id','matches.id')
.where('is_super_over','1')
.groupBy('bowler')
.orderBy('ECONOMY')
.limit('1')
.then((result) => {
    console.log(result)
})
.catch((err) => {
    console.error(err)
})
.finally( () => {
    knex.destroy()
})

