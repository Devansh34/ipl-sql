const knex=require('./connectionToDatabase')

knex
  .select("winner as Team","season")
  .count("winner as numOfMatches")
  .from("matches")
  .where("result", "normal")
  .groupBy("season","winner")
  .orderBy("winner", "season")
  .then((rows) => {
    console.log(rows);
  })
  .catch((error) => {
    console.error(error);
  })
  .finally(() => {
    knex.destroy();
  });