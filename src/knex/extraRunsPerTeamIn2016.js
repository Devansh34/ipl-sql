const knex = require("./connectionToDatabase");

knex
  .select("bowling_team")
  .sum("extra_runs as extrRunsPerYear")
  .from("deliveries")
  .join("matches", "deliveries.match_id", "=", "matches.id")
  .where('matches.season','2016')
  .groupBy('bowling_team')
  .then((result) => {
    console.log(result)
  })
  .catch((err) => {
     console.error(err)

  })
  .finally(() => {
    knex.destroy()
  });
