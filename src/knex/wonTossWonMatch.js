const knex = require("./connectionToDatabase");

knex
  .select("winner as Team")
  .select(knex.raw("count(*) As TossWonMatchWon"))
  .from("matches")
  .whereRaw("toss_winner = winner")
  .groupBy("Team")
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.error(err);
  })
  .finally(() => {
    knex.destroy();
  });
