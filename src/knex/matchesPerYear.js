const knex=require('./connectionToDatabase')

knex
  .select("season")
  .count("* as matches_count")
  .from("matches")
  .groupBy("season")
  .orderBy("season")
  .then((rows) => {
    console.log(rows);
  })
  .catch((error) => {
    console.error(error);
  })
  .finally(() => {
    knex.destroy();
  });