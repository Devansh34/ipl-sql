const knex = require("./connectionToDatabase");

knex
  .select("season")
  .select("player_of_match")
  .count("*")
  .from("matches")
  .groupBy("season", "player_of_match")
  .havingRaw(
    "count(*)=(SELECT count(*) FROM matches AS m2 WHERE matches.season=m2.season GROUP BY player_of_match ORDER BY count(*) DESC LIMIT 1)"
  )
  .orderBy("season")
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.error(err);
  })
  .finally(() => {
    knex.destroy();
  });
