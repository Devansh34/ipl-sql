const sql = require("mysql2/promise");
const deliveries = require("./data/deliveries.json");

const connection = sql.createPool({
  host: "localhost",
  user: "root",
  database: "IPL",
});

const createDeliveriesTable = 
       ` CREATE TABLE IF NOT EXISTS deliveries(
            match_id INT,
            inning INT NOT NULL,
            batting_team VARCHAR(255) NOT NULL,
            bowling_team VARCHAR(255) NOT NULL,
            \`over\` INT NOT NULL,
            ball INT NOT NULL,
            batsman VARCHAR(255) NOT NULL,
            non_striker VARCHAR(255) NOT NULL,
            bowler VARCHAR(255) NOT NULL,
            is_super_over INT NOT NULL,
            wide_runs INT NOT NULL,
            bye_runs INT NOT NULL,
            legbye_runs INT NOT NULL,
            noball_runs INT NOT NULL,
            penalty_runs INT NOT NULL,
            batsman_runs INT NOT NULL,
            extra_runs INT NOT NULL,
            total_runs INT NOT NULL,
            player_dismissed VARCHAR(255),
            dismissal_kind VARCHAR(255),
            fielder VARCHAR(255)
        );`

const insertDataIntoTable = `INSERT INTO deliveries VALUES ?;`

connection.getConnection()
.then(() => {
    console.log("Database Connected")
    return connection.query(createDeliveriesTable)
})
.then(() => {
    return connection.query(insertDataIntoTable, [
        deliveries.map((ele) => Object.values(ele))
    ]) 
})
.catch((err) => {
    console.log(err.message)
})
.finally(() => {
    connection.end()
})