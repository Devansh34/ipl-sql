USE IPL;

SELECT winner AS Team,season,count(winner) AS numOfMatches  FROM matches
WHERE result LIKE "normal"
GROUP BY season,winner
ORDER BY winner,season;