USE IPL;

SELECT season,player_of_match,count(*) FROM matches
GROUP BY season,player_of_match
HAVING count(*)=(SELECT count(*) FROM matches AS m2 WHERE matches.season=m2.season GROUP BY player_of_match
ORDER BY COUNT(*) DESC LIMIT 1) ORDER BY season