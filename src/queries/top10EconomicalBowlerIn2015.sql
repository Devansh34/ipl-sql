USE IPL;

SELECT bowler,SUM(total_runs)-SUM(legbye_runs)-SUM(bye_runs) AS runs,
COUNT(*) - SUM(CASE WHEN noball_runs>0 OR wide_runs>0 THEN 1 ELSE 0 END) AS balls,
((SUM(total_runs)-SUM(legbye_runs)-SUM(bye_runs))/((COUNT(*) - SUM(CASE WHEN noball_runs>0 OR wide_runs>0 THEN 1 ELSE 0 END))/6)) AS ECONOMY
FROM deliveries JOIN matches ON deliveries.match_id=matches.id
WHERE matches.season LIKE '2015'
GROUP BY bowler
ORDER BY ECONOMY
LIMIT 10;