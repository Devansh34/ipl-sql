use IPL;

SELECT player_dismissed,bowler,count(*) AS count from deliveries
WHERE player_dismissed NOT LIKE "" and dismissal_kind NOT LIKE "run out"
GROUP BY player_dismissed,bowler 
ORDER BY count DESC
LIMIT 1;