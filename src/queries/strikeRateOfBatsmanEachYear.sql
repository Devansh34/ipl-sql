USE IPL;

SELECT batsman,matches.season,
(SUM(batsman_runs)/NULLIF((COUNT(*) - SUM(CASE WHEN wide_runs > 0 THEN 1 ELSE 0 END)), 0))*100 AS strike_rate 
FROM deliveries join matches on deliveries.match_id=matches.id
GROUP BY batsman,season
ORDER BY season;