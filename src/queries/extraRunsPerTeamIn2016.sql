use IPL;

SELECT bowling_team,SUM(extra_runs) AS extraRunsPerYear FROM deliveries INNER JOIN matches
ON deliveries.match_id=matches.id
WHERE matches.season="2016"
GROUP BY bowling_team;